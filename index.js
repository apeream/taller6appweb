const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

express()
  .use(express.static(path.join(__dirname, 'public')))
  .use(express.static(path.join(__dirname, 'javascript')))
  .use('/images', express.static(path.join(__dirname, 'cypress/screenshots')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .get('/lists', (req, res) => res.render('pages/lists'))
  .get('/exec', runProcess)
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))

function runProcess(req, res) {
  execCypress();
  res.send("Espere un momento...");
}  

async function execCypress() {
  const cypress = require('cypress')

  await cypress.run()
  .then((results) => {
    console.log(results);
    execResemble();
  })
  .catch((err) => {
    console.error(err)
  });
}


async function execResemble(){
  const compareImages = require('resemblejs/compareImages');
  const fs = require("mz/fs");
      const options = {
        output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: 'movement',
            transparency: 0.3,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true
        },
        scaleToSameSize: true,
        ignore: ['nothing', 'less', 'antialiasing', 'colors', 'alpha'],
    };

    // The parameters can be Node Buffers
    // data is the same as usual with an additional getBuffer() function
    const data = await compareImages(
        await fs.readFile('./cypress/screenshots/1.png'),
        await fs.readFile('./cypress/screenshots/2.png'),
        options
    );

    await fs.writeFile('./cypress/screenshots/comp.png', data.getBuffer());
}

