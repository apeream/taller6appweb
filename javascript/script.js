function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function loadXMLDoc() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
         if (xmlhttp.status == 200) {
             document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
         }
         else if (xmlhttp.status == 400) {
            alert('There was an error 400');
         }
         else {
             alert('something else other than 200 was returned');
         }
      }
  };

  xmlhttp.open("GET", "exec", true);
  xmlhttp.send();
  await sleep(15000);
  window.location.reload(true); 
}
